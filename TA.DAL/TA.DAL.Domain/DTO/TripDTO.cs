﻿using System;

namespace TA.DAL.Domain.DTO
{
    public class TripDTO
    {
        public int Id { get; set; }

        public string TouristName { get; set; }

        public string CountryName { get; set; }

        public DateTime TripDate { get; set; }
    }
}
