﻿namespace TA.DAL.Domain.DTO
{
    public class TouristDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
