﻿namespace TA.DAL.Domain.DTO
{
    public class TripSummaryDTO
    {
        public string TouristName { get; set; }

        public string CountryName { get; set; }

        public int Year { get; set; }

        public int TripCount { get; set; }
    }
}
