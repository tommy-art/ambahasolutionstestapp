﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TA.DAL.Core;

namespace TA.DAL.Domain.Entities
{
    [Table("Trips")]
    public class Trip : BaseEntity<int>
    {
        [ForeignKey("Tourist")]
        public int TouristId { get; set; }

        public virtual Tourist Tourist { get; set; }

        [ForeignKey("Country")]
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }

        public DateTime TripDate { get; set; }
    }
}
