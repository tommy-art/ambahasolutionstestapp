﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TA.DAL.Core;

namespace TA.DAL.Domain.Entities
{
    [Table("Countries")]
    public class Country: BaseEntity<int>
    {
        [Required, MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }
    }
}
