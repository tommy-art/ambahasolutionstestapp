﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TA.DAL.Core;

namespace TA.DAL.Domain.Entities
{
    [Table("Tourists")]
    public class Tourist : BaseEntity<int>
    {
        [Required, MaxLength(100)]
        public string Name { get; set; }
    }
}
