﻿using Microsoft.EntityFrameworkCore;
using TA.DAL.Domain.Entities;

namespace TA.DAL.Domain
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<Tourist> Tourists { get; set; }
        public DbSet<Trip> Trips { get; set; }
    }
}
