﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using TA.DAL.Domain.Entities;

namespace TA.DAL.Domain
{
    public class SeedData
    {
        public static void InitializeData(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                var countries = new[]
                {
                    new Country
                    {
                        Name = "Russia",
                        Description = "Russia is a largest country in the world by total area"
                    },
                    new Country
                    {
                        Name = "USA",
                        Description = "The United States is the world's third- or fourth-largest country by total area"
                    }
                };

                if (!context.Countries.Any())
                {
                    context.Countries.AddRange(countries);
                    context.SaveChanges();
                }

                var tourists = new[]
                {
                    new Tourist { Name = "Kevin" },
                    new Tourist { Name = "Alex" }
                };

                if (!context.Tourists.Any())
                {
                    context.Tourists.AddRange(tourists);
                    context.SaveChanges();
                }

                var trips = new[]
                {
                    new Trip { Country = countries[0], Tourist = tourists[0], TripDate = DateTime.Parse("2018/03/31") },
                    new Trip { Country = countries[0], Tourist = tourists[0], TripDate = DateTime.Parse("2018/06/15") },
                    new Trip { Country = countries[1], Tourist = tourists[0], TripDate = DateTime.Parse("2018/01/15") },
                    new Trip { Country = countries[1], Tourist = tourists[1], TripDate = DateTime.Parse("2016/01/11") },
                    new Trip { Country = countries[1], Tourist = tourists[1], TripDate = DateTime.Parse("2017/05/10") }
                };

                if (!context.Trips.Any())
                {
                    context.Trips.AddRange(trips);
                    context.SaveChanges();
                }
            }
        }
    }
}
