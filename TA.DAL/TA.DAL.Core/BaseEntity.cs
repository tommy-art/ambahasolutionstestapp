﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TA.DAL.Core
{
    public abstract class BaseEntity<TId>
        where TId: IConvertible
    {
        [Key]
        public TId Id { get; set; }
    }
}
