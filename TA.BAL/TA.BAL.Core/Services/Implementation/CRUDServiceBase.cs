﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TA.BAL.Core.Services.Implementation
{
    public abstract class CRUDServiceBase<TEntityReadDTO, TEntityEditDTO, TKey> : IReadService<TEntityReadDTO, TKey>
        where TEntityReadDTO : class
        where TKey : IConvertible
    {
        protected readonly IReadService<TEntityReadDTO, TKey> _readService;

        protected CRUDServiceBase(
            IReadService<TEntityReadDTO, TKey> readService)
        {
            _readService = readService;
        }

        public Task<TEntityReadDTO> GetAsync(TKey id)
        {
            return _readService.GetAsync(id);
        }

        public Task<List<TEntityReadDTO>> GetAllAsync()
        {
            return _readService.GetAllAsync();
        }
    }

    public abstract class CRUDServiceBase<TEntityReadDTO, TEntityEditDTO> : CRUDServiceBase<TEntityReadDTO, TEntityEditDTO, int>
        where TEntityReadDTO : class
        where TEntityEditDTO : class
    {
        protected CRUDServiceBase(IReadService<TEntityReadDTO, int> readService) : base(readService)
        {
        }
    }

    public abstract class CRUDServiceBase<TEntityDTO> : CRUDServiceBase<TEntityDTO, TEntityDTO>
        where TEntityDTO : class
    {
        protected CRUDServiceBase(IReadService<TEntityDTO, int> readService) : base(readService)
        {
        }
    }
}
