﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace TA.BAL.Core.Services.Implementation
{
    public abstract class DataService
    {
        protected readonly DbContext _context;
        protected readonly IMapper _mapper;
        private DbContext context;

        protected DataService(DbContext context)
        {
            this.context = context;
        }

        protected DataService(
            DbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
