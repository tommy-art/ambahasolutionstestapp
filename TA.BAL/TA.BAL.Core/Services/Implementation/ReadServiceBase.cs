﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TA.DAL.Core;

namespace TA.BAL.Core.Services.Implementation
{
    public abstract class ReadServiceBase<TEntity, TEntityDTO, TKey> : DataService, IReadService<TEntityDTO, TKey>
        where TEntity : BaseEntity<TKey>
        where TEntityDTO : class
        where TKey : IConvertible
    {
        protected ReadServiceBase(
            DbContext context,
            IMapper mapper) : base(context, mapper)
        {
        }

        public virtual async Task<TEntityDTO> GetAsync(TKey id)
        {
            var entity = await _context.Set<TEntity>().FindAsync(id);
            return _mapper.Map<TEntityDTO>(entity);
        }

        public virtual Task<List<TEntityDTO>> GetAllAsync()
        {
            return GetQueryable().ProjectTo<TEntityDTO>(_mapper.ConfigurationProvider).ToListAsync(); ;
        }

        protected virtual IQueryable<TEntity> GetQueryable() => _context.Set<TEntity>();
    }

    public abstract class ReadServiceBase<TEntity, TEntityDTO> : ReadServiceBase<TEntity, TEntityDTO, int>
        where TEntity : BaseEntity<int>
        where TEntityDTO : class
    {
        protected ReadServiceBase(DbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
