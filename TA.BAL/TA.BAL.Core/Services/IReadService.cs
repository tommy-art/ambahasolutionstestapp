﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TA.BAL.Core.Services
{
    public interface IReadService<TEntityDTO, TKey>
        where TEntityDTO : class
        where TKey : IConvertible
    {
        Task<TEntityDTO> GetAsync(TKey id);

        Task<List<TEntityDTO>> GetAllAsync();
    }

    public interface IReadService<TEntityDTO> : IReadService<TEntityDTO, int>
        where TEntityDTO : class
    {

    }
}
