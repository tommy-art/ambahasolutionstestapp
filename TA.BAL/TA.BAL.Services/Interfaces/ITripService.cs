﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TA.BAL.Core.Services;
using TA.DAL.Domain.DTO;

namespace TA.BAL.Services.Interfaces
{
    public interface ITripReadService : IReadService<TripDTO>
    {
        Task<List<TripSummaryDTO>> GetTrips(int touristId, int? year);
    }

    public interface ITripService : ITripReadService
    {

    }
}
