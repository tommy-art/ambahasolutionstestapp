﻿using TA.BAL.Core.Services;
using TA.DAL.Domain.DTO;

namespace TA.BAL.Services.Interfaces
{
    public interface ICountryReadService : IReadService<CountryDTO> { }

    public interface ICountryService : ICountryReadService
    {
    }
}
