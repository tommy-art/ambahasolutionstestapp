﻿using TA.BAL.Core.Services;
using TA.DAL.Domain.DTO;

namespace TA.BAL.Services.Interfaces
{
    public interface ITouristReadService : IReadService<TouristDTO> { }

    public interface ITouristService : ITouristReadService
    {
    }
}
