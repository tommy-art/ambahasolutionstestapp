﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using TA.BAL.Core.Services.Implementation;
using TA.BAL.Services.Interfaces;
using TA.DAL.Domain;
using TA.DAL.Domain.DTO;
using TA.DAL.Domain.Entities;

namespace TA.BAL.Services.Classes
{
    public class TripReadService : ReadServiceBase<Trip, TripDTO>, ITripReadService
    {
        public TripReadService(DataContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public Task<List<TripSummaryDTO>> GetTrips(int touristId, int? year)
        {
            return _context.Set<Trip>()
                .Where(x => x.TouristId == touristId && (year == null || x.TripDate.Year == year))
                .GroupBy(x => new
                {
                    x.TouristId,
                    TouristName = x.Tourist.Name,
                    x.CountryId,
                    CountryName = x.Country.Name,
                    x.TripDate.Year
                })
                .Select(g => new TripSummaryDTO
                {
                    TouristName = g.Key.TouristName,
                    CountryName = g.Key.CountryName,
                    Year = g.Key.Year,
                    TripCount = g.Count()
                }).ToListAsync();
        }
    }

    public class TripService : CRUDServiceBase<TripDTO>, ITripService
    {
        private readonly ITripReadService _tripReadService;

        public TripService(ITripReadService readService) : base(readService)
        {
            _tripReadService = readService;
        }

        public Task<List<TripSummaryDTO>> GetTrips(int touristId, int? year)
        {
            return _tripReadService.GetTrips(touristId, year);
        }
    }
}
