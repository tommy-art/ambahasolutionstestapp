﻿using AutoMapper;
using TA.BAL.Core.Services.Implementation;
using TA.BAL.Services.Interfaces;
using TA.DAL.Domain;
using TA.DAL.Domain.DTO;
using TA.DAL.Domain.Entities;

namespace TA.BAL.Services.Classes
{
    public class CountryReadService : ReadServiceBase<Country, CountryDTO>, ICountryReadService
    {
        public CountryReadService(DataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }

    public class CountryService : CRUDServiceBase<CountryDTO>, ICountryService
    {
        public CountryService(ICountryReadService readService) : base(readService)
        {
        }
    }
}
