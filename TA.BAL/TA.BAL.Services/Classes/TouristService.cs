﻿using AutoMapper;
using TA.BAL.Core.Services.Implementation;
using TA.BAL.Services.Interfaces;
using TA.DAL.Domain;
using TA.DAL.Domain.DTO;
using TA.DAL.Domain.Entities;

namespace TA.BAL.Services.Classes
{
    public class TouristReadService : ReadServiceBase<Tourist, TouristDTO>, ITouristReadService
    {
        public TouristReadService(DataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }

    public class TouristService : CRUDServiceBase<TouristDTO>, ITouristService
    {
        public TouristService(ITouristReadService readService) : base(readService)
        {
        }
    }
}
