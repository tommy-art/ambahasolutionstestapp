﻿using AutoMapper;
using TA.DAL.Domain.DTO;
using TA.DAL.Domain.Entities;

namespace TA.BAL.Services.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Country, CountryDTO>();
            CreateMap<Tourist, TouristDTO>();
            CreateMap<Trip, TripDTO>();
        }
    }
}
