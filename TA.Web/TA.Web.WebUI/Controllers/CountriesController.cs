﻿using Microsoft.AspNetCore.Mvc;
using TA.BAL.Services.Interfaces;
using TA.DAL.Domain.DTO;
using TA.Web.Core;

namespace TA.Web.WebUI.Controllers
{
    [Route("api/countries")]
    public class CountriesController : CRUDControllerBase<CountryDTO>
    {
        private readonly ICountryService _countryService;

        public CountriesController(ICountryService countryService) : base(countryService)
        {
            _countryService = countryService;
        }
    }
}
