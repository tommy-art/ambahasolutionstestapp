﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TA.BAL.Services.Interfaces;
using TA.DAL.Domain.DTO;
using TA.Web.Core;

namespace TA.Web.WebUI.Controllers
{
    [Route("api/trips")]
    public class TripsController : CRUDControllerBase<TripDTO>
    {
        private readonly ITripService _tripService;

        public TripsController(ITripService tripService) : base(tripService)
        {
            _tripService = tripService;
        }

        [HttpGet, Route("{touristId}/{year?}")]
        public Task<IActionResult> GetTrips(int touristId, int? year)
        {
            async Task<IActionResult> func()
            {
                return Ok(await _tripService.GetTrips(touristId, year));
            }

            return Task.Run(func);
        }
    }
}
