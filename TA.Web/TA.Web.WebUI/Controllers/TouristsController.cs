﻿using Microsoft.AspNetCore.Mvc;
using TA.BAL.Services.Interfaces;
using TA.DAL.Domain.DTO;
using TA.Web.Core;

namespace TA.Web.WebUI.Controllers
{
    [Route("api/tourists")]
    public class TouristsController : CRUDControllerBase<TouristDTO>
    {
        public TouristsController(ITouristService touristService) : base(touristService)
        {
        }
    }
}
