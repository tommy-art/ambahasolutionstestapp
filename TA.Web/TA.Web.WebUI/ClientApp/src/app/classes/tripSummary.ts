export class TripSummary {
  touristName: string;
  countryName: string;
  year: number;
  tripCount: number;
}
