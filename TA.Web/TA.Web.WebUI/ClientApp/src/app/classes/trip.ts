export class Trip {
  id: number;
  touristName: string;
  countryName: string;
  tripDate: Date;
}
