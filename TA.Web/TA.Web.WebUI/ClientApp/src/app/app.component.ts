import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  items: MenuItem[];

  ngOnInit(): void {
    this.items = [
      { label: 'Список стран', icon: 'fa fa-fw fa-book', routerLink: ['/country-list'] },
      { label: 'Отчет о посещаемости', icon: 'fa fa-fw fa-bar-chart', routerLink: ['/trip-report'] }
    ];
  }
}
