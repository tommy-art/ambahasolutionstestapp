import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { TabMenuModule } from 'primeng/tabmenu';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { ButtonModule } from 'primeng/button'

import { AppComponent } from './app.component';
import {
  CountryListComponent,
  TripReportComponent
} from './components/index';


@NgModule({
  declarations: [
    AppComponent,
    CountryListComponent,
    TripReportComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'country-list', component: CountryListComponent },
      { path: 'trip-report', component: TripReportComponent },
      { path: '**', redirectTo: '/country-list' }
    ]),
    TabMenuModule,
    TableModule,
    CardModule,
    DropdownModule,
    CalendarModule,
    ButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
