import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReadService } from './base/read.service';
import { Country } from '../classes/country';

@Injectable()
export class CountryService extends ReadService<Country> {
  protected url = "/api/countries";

  constructor(http: HttpClient) {
    super(http);
  }
}
