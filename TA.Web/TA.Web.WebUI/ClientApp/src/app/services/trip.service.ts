import { Injectable } from '@angular/core';
import { ReadService } from './base/read.service';
import { Trip } from '../classes/trip';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TripSummary } from '../classes/tripSummary';

@Injectable()
export class TripService extends ReadService<Trip> {
  protected url = "api/trips";

  constructor(protected http: HttpClient) {
    super(http);
  }

  getTrips(touristId: number, year?: number): Observable<TripSummary[]> {
    return this.http.get<TripSummary[]>(`${this.url}/${touristId}/${year}`);
  }
}
