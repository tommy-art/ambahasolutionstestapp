import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

export abstract class ReadService<TEntity> {
  protected abstract url: string;

  constructor(protected http: HttpClient) { }

  get(id: number | string): Observable<TEntity> {
    return this.http.get<TEntity>(`${this.url}/${id}`);
  }

  getAll(): Observable<TEntity[]> {
    return this.http.get<TEntity[]>(`${this.url}/all`);
  }
}
