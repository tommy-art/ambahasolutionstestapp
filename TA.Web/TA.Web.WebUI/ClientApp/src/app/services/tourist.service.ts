import { Injectable } from '@angular/core';
import { ReadService } from './base/read.service';
import { Tourist } from '../classes/tourist';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TouristService extends ReadService<Tourist> {
  protected url = "/api/tourists";

  constructor(http: HttpClient) {
    super(http);
  }
}
