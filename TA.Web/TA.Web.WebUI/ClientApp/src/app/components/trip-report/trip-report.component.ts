import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Tourist } from '../../classes/tourist';
import { TripSummary } from '../../classes/tripSummary';
import { TouristService } from '../../services/tourist.service';
import { TripService } from '../../services/trip.service';

@Component({
  selector: 'app-trip-report',
  templateUrl: './trip-report.component.html',
  styleUrls: ['./trip-report.component.css'],
  providers: [TouristService, TripService]
})
export class TripReportComponent implements OnInit {
  touristItems: SelectItem[];
  maxDateValue = new Date();

  tripSummaries: TripSummary[];

  filter: ITripFilter;

  constructor(private touristService: TouristService, private tripService: TripService) {
    this.initFilter();
  }

  search(): void {
    this.tripService.getTrips(
      this.filter.tourist.id,
      this.filter.tripDate ? this.filter.tripDate.getFullYear() : null
    )
      .subscribe(trips => this.tripSummaries = trips);
  }

  clear(): void {
    this.initFilter();
    this.tripSummaries = [];
  }

  private initFilter(): void {
    this.filter = {
      tourist: null,
      tripDate: null
    }
  }

  ngOnInit() {
    this.touristItems = [];
    this.touristService.getAll().subscribe(tourists => {
      tourists.forEach(tourist => this.touristItems.push({ label: tourist.name, value: tourist }));
    })

    let today = new Date();

    this.maxDateValue.setDate(today.getDate() + 1);
  }
}

interface ITripFilter {
  tourist: Tourist;
  tripDate: Date;
}
