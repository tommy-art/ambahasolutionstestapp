import { Component, OnInit } from '@angular/core';
import { CountryService } from '../../services/country.service';
import { Country } from '../../classes/country';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css'],
  providers: [CountryService]
})
export class CountryListComponent implements OnInit {
  countries: Country[];

  constructor(private countryService: CountryService) {
    
  }

  ngOnInit() {
    this.countryService.getAll().subscribe(countries => this.countries = countries);
  }

}
