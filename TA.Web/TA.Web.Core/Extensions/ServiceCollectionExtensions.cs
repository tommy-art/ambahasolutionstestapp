﻿using Microsoft.Extensions.DependencyInjection;
using TA.BAL.Services.Classes;
using TA.BAL.Services.Interfaces;

namespace TA.Web.Core.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddBusinessServices(this IServiceCollection services)
        {
            services.AddTransient<ICountryReadService, CountryReadService>();
            services.AddTransient<ICountryService, CountryService>();

            services.AddTransient<ITouristReadService, TouristReadService>();
            services.AddTransient<ITouristService, TouristService>();

            services.AddTransient<ITripReadService, TripReadService>();
            services.AddTransient<ITripService, TripService>();
            return services;
        }
    }
}
