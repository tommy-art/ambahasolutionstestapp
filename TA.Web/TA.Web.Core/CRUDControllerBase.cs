﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TA.BAL.Core.Services;

namespace TA.Web.Core
{
    public abstract class CRUDControllerBase<TEntityDTO, TKey> : Controller
        where TEntityDTO : class
        where TKey : IConvertible
    {
        protected IReadService<TEntityDTO, TKey> _readService;

        protected CRUDControllerBase(
            IReadService<TEntityDTO, TKey> readService
            )
        {
            _readService = readService;
        }

        [HttpGet, Route("{id}")]
        public virtual Task<IActionResult> Get(TKey id)
        {
            async Task<IActionResult> func()
            {
                var res = await _readService.GetAsync(id);
                if (res == null)
                    return NotFound();
                return Ok(res);
            }

            return Task.Run(func);
        }

        [HttpGet, Route("all")]
        public virtual Task<IActionResult> GetAll()
        {
            async Task<IActionResult> func()
            {
                return Ok(await _readService.GetAllAsync());
            }

            return Task.Run(func);
        }
    }

    public abstract class CRUDControllerBase<TEntityDTO> : CRUDControllerBase<TEntityDTO, int>
        where TEntityDTO : class
    {
        protected CRUDControllerBase(IReadService<TEntityDTO, int> readService) : base(readService)
        {
        }
    }
}
